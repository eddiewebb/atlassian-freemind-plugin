# Atlassian Freemind Plugin

This plugin was written by Edward Webb and is not affiliated with Freemind [1] or Atlassin.

THis product is a bridge between the two products displaying an interactive rendering of Freemind Mindmaps in confluence.



## Easy to configure
> Just point to a page that has a .mm attachment, and choose it from the auto-complete dropdown.

**Attachments can be form different pages and spaces ! **

![Image of configuring a macro](/eddiewebb/atlassian-freemind-plugin/raw/57d5a7a87690/src/main/resources/images/config.png "Easy Configuration")

![Image of macro block in editor](/eddiewebb/atlassian-freemind-plugin/raw/57d5a7a87690/src/main/resources/images/macro_block.png "Easy to update")


## Interactive Display
> You can _scroll_, _zoom_, _select_ and _expand_

![Image of embedded mindmap](/eddiewebb/atlassian-freemind-plugin/raw/57d5a7a87690/src/main/resources/images/view.png "Interactive Display")


## Problems Moving pages or attachments
> If you move, rename or delete an attachment used by a Freemind-Macro, you will see the following error:

![Error rendering macro 'atlassian-freemind-macro' : The specified attachment 'Any Freemind Map.mm' could not be found, please verify the page and file name.](/eddiewebb/atlassian-freemind-plugin/raw/57d5a7a87690/src/main/resources/images/missing_attachment.png "Unable to locate attachment")


## Licenses
 
### Freemind
This plugin uses binaries provided by Freemind[1] under the GPLv2. That license can be found in this software installation.


[1]: http://freemind.sourceforge.net "FreeMind - free mind mapping software"
