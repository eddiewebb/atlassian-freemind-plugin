package it;

/**
 * Does not use Junit, so @Test will not work. Be sure to prefix all tests methods.
 * @author Edward A. Webb
 */
public class IntegrationTestFreemindMacro extends AbstractIntegrationTestCase
{
	
	public void testAMapSpecifiedInAPageIsVisible()
	{
        gotoPage("display/FREEMIND/PageWithoutSpacesWithMap");
        assertTextPresent("This browser does not have a Java Plug-in");
        assertTextNotPresent("could not be found, please verify the page and file name");
        
        assertElementPresentByXPath("//object");
	}
		
	public void testAMapSpecifiedInAPageWithSPacesIsVisible()
	{
        gotoPage("display/FREEMIND/Page+With+Spaces+And+Map");
        assertTextPresent("This browser does not have a Java Plug-in");
        assertTextNotPresent("could not be found, please verify the page and file name");
	}	
	
	public void testAMapSpecifiedInADifferentPageIsVisible()
	{
        gotoPage("display/FREEMIND/PageWithoutSpacesPullsMap");
        assertTextPresent("This browser does not have a Java Plug-in");
        assertTextNotPresent("could not be found, please verify the page and file name");
	}
		
	public void testAMapSpecifiedInADifferentPageContainingSpacesIsVisible()
	{
        gotoPage("display/FREEMIND/Page+With+Spaces+Pulls+Map");
        assertTextPresent("This browser does not have a Java Plug-in");
        assertTextNotPresent("could not be found, please verify the page and file name");
	}
}
