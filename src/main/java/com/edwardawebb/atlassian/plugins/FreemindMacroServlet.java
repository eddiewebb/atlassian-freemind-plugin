/**
 * Copyright 2013 
 * atlassian-freemind-plugin - 
 *
 */
package com.edwardawebb.atlassian.plugins;

import java.io.IOException;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.confluence.pages.Attachment;

/**
 * @author Edward Webb
 *
 */
public class FreemindMacroServlet extends HttpServlet{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1479915356590125520L;
	private FreemindMacroService service;

	public FreemindMacroServlet(FreemindMacroService service){
		this.service = service;
	}


	


	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String uuid = req.getParameter("uuid");
		try{
			Attachment attachment = service.retrieveAttachment(UUID.fromString(uuid));
			org.apache.commons.io.IOUtils.copy(attachment.getContentsAsStream(),resp.getOutputStream());
		}catch(NullPointerException npe){
			// expected if same url is hit twice, or bogus url is hit
			resp.getWriter().println("Oops, that attachment has come and gone.");
		}
	}









	
	
	


}
