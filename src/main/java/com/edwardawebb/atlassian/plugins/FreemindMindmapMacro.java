/**
 * Copyright Oct 4, 2012 Edward A. Webb
 * 
 */package com.edwardawebb.atlassian.plugins;

import java.io.IOException;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.velocity.VelocityContext;
//import org.slf4j.LoggerFactory;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import org.apache.velocity.context.Context;

/**
 * Trivial plugin creates the proper URL for the freemind-browser jar 
 * and attachment, along with default macro dimensions.
 * @author Eddie Webb
 */
public class FreemindMindmapMacro implements Macro
{
	//private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(FreemindMindmapMacro.class);

	  private static final String TEMPLATE = "templates/freemind-applet.vm";
	  private static final String TEMPLATE_ERROR = "templates/freemind-error.vm";

	private static final String DEFAULT_WIDTH = "600px";

	private static final String DEFAULT_HEIGHT = "400px";


	private final AttachmentManager attachmentManager;

	private final SettingsManager settingsManager;

	private FreemindMacroService freemindMacroService;

	/**
	 *
	 * @param attachmentManager
	 * @param settingsManager
	 * @param pageManager
	 */
    public FreemindMindmapMacro( AttachmentManager attachmentManager,SettingsManager settingsManager,FreemindMacroService freemindMacroService)
    {
        this.attachmentManager = attachmentManager;
        this.settingsManager = settingsManager;
        this.freemindMacroService = freemindMacroService;
    }

    @Override
    public String execute(Map<String, String> parameters, String body, ConversionContext context) throws MacroExecutionException
    {

    	Attachment attachment = attachmentManager.getAttachment(context.getEntity(), parameters.get("name"));

		Map<String, Object> contextMap = MacroUtils.defaultVelocityContext();
		contextMap.put("width", parameters.get("width") != null ? parameters.get("width") : DEFAULT_WIDTH);
		contextMap.put("height", parameters.get("height") != null ? parameters.get("height") : DEFAULT_HEIGHT);
		contextMap.put("attachmentName",  parameters.get("name"));
		//contextMap.put("toolbar",  parameters.get("toolbar"));
		contextMap.put("baseUrl",  settingsManager.getGlobalSettings().getBaseUrl() );

		//contextMap.put("attachmentEncodedUri",  attachmentAsEncodedUri(context.getEntity(), parameters.get("name")));
		contextMap.put("attachmentEncodedUri",  settingsManager.getGlobalSettings().getBaseUrl() + freemindMacroService.exposeAttachment(attachment) );
		return VelocityUtils.getRenderedTemplate(TEMPLATE, contextMap);
    }

    @Override
    public BodyType getBodyType()
    {
        return BodyType.NONE;
    }



    @Override
    public OutputType getOutputType()
    {
        return OutputType.BLOCK;
    }
    /**
     * this is required since the applet uses a different user agent string which breaks authorization for
     * attachments on restricted pages.
     * @param page
     * @param attachmentName
     * @return
     * @throws IOException
     */
    private String attachmentAsEncodedUri(ContentEntityObject page,String attachmentName) throws IOException{
    	Attachment attachment = attachmentManager.getAttachment(page,attachmentName);
    	String result = encodeXml(attachment);
    	return result;
    }

	private String encodeXml(Attachment attachment) throws IOException {
		byte[] bytes = IOUtils.toByteArray(attachment.getContentsAsStream());

		byte[] base64 = Base64.encodeBase64(bytes);

		StringBuilder dataUri = new StringBuilder();
		dataUri.append("data:");
		dataUri.append("text/xml");
		dataUri.append(";base64,");
		dataUri.append(new String(base64,0,base64.length-1));
		return dataUri.toString().trim();
	}

}
