/**
 * Copyright 2013 
 * atlassian-freemind-plugin - 
 *
 */
package com.edwardawebb.atlassian.plugins;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import com.atlassian.confluence.pages.Attachment;

/**
 * @author Edward Webb
 *
 */
public class FreemindMacroService {
	public static String SERVLET_PATH="/plugins/servlet/freemindattachment";
	
	private Map<UUID,Attachment> exposedAttachments = new HashMap<UUID,Attachment>();

	public synchronized String exposeAttachment(Attachment attachment){
		UUID key = UUID.randomUUID();
		exposedAttachments.put(key,attachment);
		return SERVLET_PATH + "?uuid=" + key.toString() + "&format=.mm";
	}
	
	public synchronized Attachment retrieveAttachment(UUID key){
		Attachment attachment = exposedAttachments.get(key);
		exposedAttachments.remove(key);
		return attachment;
	}
	
}
